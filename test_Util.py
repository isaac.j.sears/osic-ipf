from unittest import TestCase
from Util import *
from DicomStorageManager import *

import numpy as np
import tensorflow as tf

DATA_PATH = './data/unittest'
TEST_PT = 'ID00007637202177411956430'



class Test(TestCase):

    def setUp(self) -> None:
        self.test_feature = load_dicom(TEST_PT, 12)
        df_in = pd.read_csv('./data/train.csv')
        self.test_value = df_in[df_in.Patient == TEST_PT]['Percent'].mean() / 100

    def test_training_ex_to_tfrecord(self):
        training_ex_to_tfrecord(self.test_feature, self.test_value, './data/unittest/test.tfrecord')
        dataset = tf.data.TFRecordDataset(['./data/unittest/test.tfrecord'])
        dataset = dataset.map(parse_function)

        x, y_actual = iter(dataset).get_next()

        assert tf.equal(x, self.test_feature).numpy().all()
        assert tf.equal(y_actual, self.test_value).numpy().all()

