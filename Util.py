import numpy as np
import pandas as pd
import tensorflow as tf

MAX_AGE = 120
MAX_FVC = 4000
MAX_WEEK = 133
MIN_WEEK = -12

def _bytes_feature(value):
  """Returns a bytes_list from a string / byte."""
  if isinstance(value, type(tf.constant(0))):
    value = value.numpy() # BytesList won't unpack a string from an EagerTensor.
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def _float_feature(value):
  """Returns a float_list from a float / double."""
  return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def _int64_feature(value):
  """Returns an int64_list from a bool / enum / int / uint."""
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def training_ex_to_tfrecord(x: np.ndarray, y_actual: float, path: str):
    x_serialized = _bytes_feature(tf.io.serialize_tensor(x))
    x_num_slices_serialized = _int64_feature(np.shape(x)[0]),
    x_height_serialized = _int64_feature(np.shape(x)[1]),
    x_width_serialized = _int64_feature(np.shape(x)[2]),
    y_actual_serialized = _float_feature(y_actual)


    example_proto = tf.train.Example(features=tf.train.Features(feature={
        'x': x_serialized,
        'x_num_slices': x_num_slices_serialized,
        'x_height': x_height_serialized,
        'x_width': x_width_serialized,
        'y_actual': y_actual_serialized
    }))

    out = example_proto.SerializeToString()

    with tf.io.TFRecordWriter(path) as writer:
        writer.write(out)


def get_parse_function(shape):
    return ParseFunctionFactory(shape).parse_function


def parse_function(example_proto):
    tf_record_format = {
        'x': tf.io.FixedLenFeature([], tf.string, default_value=''),
        # 'x_num_slices': tf.io.FixedLenFeature((1,), tf.int64),
        # 'x_height': tf.io.FixedLenFeature((1,), tf.int64),
        # 'x_width': tf.io.FixedLenFeature((1,), tf.int64),
        'y_actual': tf.io.FixedLenFeature((1,), tf.float32)
    }
    record = tf.io.parse_single_example(example_proto, tf_record_format)
    x_shape = (12, 512, 512)  # TODO: This won't work during real submission
    return tf.ensure_shape(tf.io.parse_tensor(record['x'], tf.int16), x_shape), record['y_actual']


class ParseFunctionFactory():
    def __init__(self, shape):
        self.tf_record_format = {
            'x': tf.io.FixedLenFeature([], tf.string, default_value=''),
            'x_shape': tf.io.FixedLenFeature((3,), tf.int64),
            'y_actual': tf.io.FixedLenFeature((1,), tf.float32)
        }

        self.final_shape = shape


    def parse_function(self, example_proto):
        record = tf.io.parse_single_example(example_proto, self.tf_record_format)
        x = tf.io.parse_tensor(record['x'], tf.int16)
        return tf.ensure_shape(tf.io.parse_tensor(record['x'], tf.int16), self.final_shape), record['y_actual']


def osic_evaluate(sigma, FVC_actual, FVC_predicted):
    """
    Evaluation function used in the competition: modified Laplace Log Likelihood

    :param sigma: standard deviation
    :param FVC_actual: real FVC value
    :param FVC_predicted: predicted FVC value
    :return: modified laplace log likelihood
    """

    sigma_clipped = np.maximum(sigma, 70)
    delta = np.minimum(np.absolute(FVC_actual - FVC_predicted), 1000)
    metric = -(np.sqrt(2) * delta) / sigma_clipped - np.log(np.sqrt(2) * sigma_clipped)

    return metric


def osic_custom_loss(y_true, y_pred):
    # De-normalize
    y_true = y_true * float(MAX_FVC)
    y_pred = y_pred * float(MAX_FVC)

    delta = tf.minimum(tf.abs(y_true - y_pred), 1000.)
    metric = -(tf.sqrt(2.) * delta) / 250. - tf.math.log(tf.sqrt(2.) * 250.)

    return - metric  # Inverse because trying to get closer to zero?

def extract_features_train(df_in):
    # Input matrix will be (# of entries in tabular data set x # of features)
    # Features:
    # (0) Age (1) Gender (2) Smoker-status (3) Baseline % (4) Week #
    #
    # Note:
    # Baseline % comes from percent column corresponding to EARLIEST (>= 0) WEEK
    # Week # is calculated as (Weeks column) - (EARLIEST (>= 0) WEEK)

    X = np.zeros((df_in.shape[0], 6))
    idx = 0

    # For now, just outputting a prediction. Will worry about confidence later
    Y = np.zeros((df_in.shape[0], 1))

    for pt in df_in.Patient.unique():
        pt_df = df_in[df_in.Patient == pt]
        # print(pt_df.head(5))

        # Get earliest positive week and get baseline % (same for every entry for this patient)
        earliest_week_after_baseline = pt_df[pt_df.Weeks == pt_df[pt_df.Weeks >= 0].Weeks.min()].Weeks.values[0]
        earliest_week_after_baseline_percent = pt_df[pt_df.Weeks == earliest_week_after_baseline].Percent.values[0]
        earliest_week_after_baseline_fvc = pt_df[pt_df.Weeks == earliest_week_after_baseline].FVC.values[0]

        for index, row in pt_df.iterrows():
            # Normalize age
            age_normalized = row['Age'] / MAX_AGE

            # Normalize gender
            if row['Sex'] == 'Male':
                sex_normalized = 1
            elif row['Sex'] == 'Female':
                sex_normalized = 0
            else:
                assert False, "Bad entry in Sex column: {}".format(row['Sex'])

            # Normalize smoker status
            if row['SmokingStatus'] == 'Currently smokes':
                smoke_normalized = 1
            elif row['SmokingStatus'] == 'Ex-smoker':
                smoke_normalized = 0.5
            elif row['SmokingStatus'] == 'Never smoked':
                smoke_normalized = 0
            else:
                assert False, "Bad entry in SmokingStatus column: {}".format(row['SmokingStatus'])

            # Normalize baseline %
            baseline_percent_normalized = earliest_week_after_baseline_percent / 100

            # Normalize baseline FVC
            baseline_fvc_normalized = earliest_week_after_baseline_fvc / MAX_FVC

            # Compute week relative to earliest FVC measurement and normalize
            week_actual = row['Weeks'] - earliest_week_after_baseline
            week_normalized = week_actual / MAX_WEEK

            X[idx] = np.array(
                [age_normalized, sex_normalized, smoke_normalized, baseline_percent_normalized, baseline_fvc_normalized, week_normalized])


            # Compute actual % at this time
            # Y[idx] = row['Percent'] / 100
            Y[idx] = row['FVC'] / MAX_FVC

            idx += 1

    return X, Y


def extract_features_test(df_in):
    X = np.zeros(df_in.shape[0] * (13 + 133), 6)
    expanded_df = pd.DataFrame(columns=["Patient", "Weeks", "FVC", "Percent", "Age", "Sex", "SmokingStatus"])
    x_idx = 0

    for pt in df_in.Patient.unique():
        row = df_in[df_in.Patient == pt]

        for week_idx in range(MIN_WEEK, MAX_WEEK + 1):
            expanded_df = expanded_df.append({
                "Patient_Week": pt + "_" + str(week_idx),
                "FVC": 0,  # placeholder
                "Confidence": 0 # placeholder
            }, ignore_index=True)

            # Normalize age
            age_normalized = row['Age'] / MAX_AGE

            # Normalize gender
            if row['Sex'] == 'Male':
                sex_normalized = 1
            elif row['Sex'] == 'Female':
                sex_normalized = 0
            else:
                assert False, "Bad entry in Sex column: {}".format(row['Sex'])

            # Normalize smoker status
            if row['SmokingStatus'] == 'Currently smokes':
                smoke_normalized = 1
            elif row['SmokingStatus'] == 'Ex-smoker':
                smoke_normalized = 0.5
            elif row['SmokingStatus'] == 'Never smoked':
                smoke_normalized = 0
            else:
                assert False, "Bad entry in SmokingStatus column: {}".format(row['SmokingStatus'])

            # Normalize baseline %
            percent_normalized = row['Percent'] / 100

            # Normalize baseline FVC
            fvc_normalized = row['FVC'] / MAX_FVC

            # Compute week relative to earliest FVC measurement and normalize
            week_normalized = row['Weeks'] / MAX_WEEK

            X[x_idx] = np.array([age_normalized, sex_normalized, smoke_normalized, percent_normalized, fvc_normalized, week_normalized])

            x_idx += 1

    return X, expanded_df
