import pydicom
import os
import glob
import json

import numpy as np
import pandas as pd
import tensorflow as tf

from tensorflow import keras
from tqdm.keras import TqdmCallback
from DicomGenerator import DicomGenerator
from Util import *

if 'USER' in os.environ and os.environ['USER'] == 'isears':
    DATA_ROOT = './data/train'
    MODEL_ROOT = './models'
    VERBOSE = 1
    # CALLBACKS = [TqdmCallback(verbose=0)]
    CALLBACKS = []
    ENV_KAGGLE = False
    EPOCHS = 10
    CACHE_PATH = './data/cache/train'
else:
    DATA_ROOT = '/kaggle/input/osic-pulmonary-fibrosis-progression/train'
    MODEL_ROOT = '/kaggle/input/vanilladnnmodel'
    CALLBACKS = list()
    VERBOSE = 1
    ENV_KAGGLE = True
    MODEL_ROOT = '.'
    EPOCHS = 1000

files = glob.glob(f'{CACHE_PATH}/*.tfrecord')
dataset = tf.data.TFRecordDataset(files)
dataset = dataset.map(parse_function)
dataset = dataset.shuffle(500).batch(5)


model = keras.models.Sequential()
model.add(keras.layers.Conv2D(32, kernel_size=(3, 3), activation='relu', kernel_initializer='he_uniform',
                              input_shape=(12, 512, 512)))
model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(keras.layers.Conv2D(32, kernel_size=(3, 3), activation='relu', kernel_initializer='he_uniform'))
model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
model.add(keras.layers.Flatten())
model.add(keras.layers.Dense(256, activation='relu', kernel_initializer='he_uniform'))
model.add(keras.layers.Dense(1, activation='sigmoid'))

model.compile(loss='mean_squared_error', optimizer='adam')

history = model.fit(
    dataset,
    use_multiprocessing=True,
    workers=2,
    steps_per_epoch=1,
    epochs=100,
    verbose=VERBOSE,
    callbacks=CALLBACKS,
)

print("Saving model...")
model.save(f'{MODEL_ROOT}/dicom-cnn.h5')

print(history.history)
