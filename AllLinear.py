"""
Simplest possible approach to problem: assume all decline is linear. Average all patient's R-squared values and use
that as error
"""

import pandas as pd

from sklearn.metrics import r2_score
from Util import *


# Get linear regression params

df_train = pd.read_csv("data/train.csv")

params = np.polyfit(df_train.Weeks, df_train.FVC, 1)
r2 = r2_score(df_train.FVC, np.poly1d(params)(df_train.Weeks))

print(params)
print(r2)


# Evaluate on training data
predicted_FVC = np.poly1d(params)(df_train.Weeks)
score = osic_evaluate(np.std(df_train.FVC), df_train.FVC, predicted_FVC)
print(np.mean(score))