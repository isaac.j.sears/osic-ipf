# Run to convert dicoms into .npy stored on HDD for access by keras custom data generator
import os
import pydicom
import glob

from Util import *


if 'USER' in os.environ and os.environ['USER'] == 'isears':
    LOAD_DIR = './data/train'
    OUT_DIR = './data/cache/train'

else:
    LOAD_DIR = '/kaggle/input/osic-pulmonary-fibrosis-progression/train'
    OUT_DIR = '.'


def get_min_slices(pt_list):
    all_slice_counts = [len(glob.glob(f'{LOAD_DIR}/{pt_id}/*.dcm')) for pt_id in pt_list]
    return np.min(all_slice_counts)


def load_dicom(pt_id, min_slices):
    # Load slices using dcom
    slices = [pydicom.read_file(x) for x in glob.glob(f'{LOAD_DIR}/{pt_id}/*.dcm')]
    slices.sort(key=lambda x: int(x.InstanceNumber))

    # TODO: for now, just cut down all scans to smallest # of slices in dataset
    minimized_slices = list()

    for idx in np.linspace(0, len(slices) - 1, min_slices):
        minimized_slices.append(slices[int(idx)])

    assert len(minimized_slices) == min_slices  # TODO: Everything needs to be the same size

    # Load numpy arrays from slices
    img = np.stack([s.pixel_array for s in minimized_slices])
    img = img.astype(np.int16)
    img[img == -2000] = 0

    intercept = minimized_slices[0].RescaleIntercept
    slope = minimized_slices[0].RescaleSlope

    for slice in minimized_slices:
        assert slice.RescaleIntercept == intercept, "Uh oh..."
        assert slice.RescaleSlope == slope, "Uh oh..."

    img = slope * img.astype(np.float64)
    img = img.astype(np.int16)
    img += np.int16(intercept)

    return img


if __name__ == '__main__':
    df_in = pd.read_csv(f'{LOAD_DIR}.csv')

    min_slices = get_min_slices(df_in['Patient'])
    sample_img = load_dicom(df_in.iloc[0].Patient, min_slices)
    pt_count = len(df_in.Patient.unique())
    sample_shape = np.shape(sample_img)
    drop_count = 0

    for idx, pt_id in enumerate(df_in.Patient.unique()):
        pt_df = df_in[df_in.Patient == pt_id]

        try:
            dicom = load_dicom(pt_id, min_slices)
            if np.shape(dicom) == (min_slices, 512, 512):
                # TODO: Maybe averaging isn't the best approach here. Should at least weight values closer to scan-time heavier
                y_actual = pt_df['Percent'].mean() / 100
                training_ex_to_tfrecord(dicom, y_actual, f'{OUT_DIR}/{pt_id}.tfrecord')

            else:  # TODO: should try to resize 768x768 scans
                print(f'Dropping {pt_id} because of bad dimensions')
                drop_count += 1
        except RuntimeError:
            print(f'Dropping {pt_id} because of value error while reading scan')
            drop_count += 1

    print(f'Dropped {drop_count} patients because bad dimensions')


    print("Done")