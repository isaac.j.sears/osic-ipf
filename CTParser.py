import pandas as pd


class CTParser:
    def __init__(self, csv_path, ct_scan_path):
        self.csv_path = csv_path
        self.ct_scan_path = ct_scan_path


    def ctParse(self, proto):
        """
        :param proto: Assumed to be a path pointing to one of the directories containing the dcm's that make up a CT scan
        :return: x, y_actual pair
        """
        pass

    def _getIdFromPath(self, path):
        return path.split('/')[-1]