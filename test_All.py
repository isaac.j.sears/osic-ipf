import numpy as np

from unittest import TestCase
from CTParser import *

CT_SCAN_PATH = './data/train'
TEST_ID = 'ID00007637202177411956430'
TEST_PATH = './data/train/ID00007637202177411956430'
TEST_Y = {
    'ID00020637202178344345685': np.mean(np.array([
        117.770713699754,109.977440525021,115.10459392945,115.360951599672,107.46513535685,108.439294503692,
        105.516817063167,99.4667760459393,95.4163248564397
    ])) / 100
}


class Test(TestCase):

    def setUp(self) -> None:
        self.parser = CTParser(f'{CT_SCAN_PATH}/train.csv', CT_SCAN_PATH)

    def test_ct_parse(self):
        x, y_actual = self.parser.ctParse(TEST_ID)
        self.fail()

    def test__getIdFromPath(self):
        assert self.parser._getIdFromPath(TEST_PATH) == TEST_ID

