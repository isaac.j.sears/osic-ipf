import numpy as np

from tensorflow import keras


class DicomGenerator(keras.utils.Sequence):

    def __init__(self, y_actual, x_data_path, batch_size=32, dim=(70, 512, 512), shuffle=True):
        # TODO: first dimension of dim will change (min_number_slices)
        self.dim = dim
        self.batch_size = batch_size
        self.y_actual = y_actual
        self.shuffle = shuffle
        self.data_path = x_data_path

        # ???
        self.on_epoch_end()

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples'
        # Initialization
        X = np.empty((self.batch_size, *self.dim))
        y = np.empty((self.batch_size), dtype=float)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            X[i, ] = np.load(f'{self.data_path}/{ID}.npy')

            # Store class
            y[i] = self.y_actual[ID]

        return X, y

    def __len__(self):
        return int(np.floor(len(self.y_actual) / self.batch_size))

    def __getitem__(self, index):
        low = index * self.batch_size
        high = (index + 1) * self.batch_size
        indexes = self.indexes[low:high]
        list_IDs_temp = [list(self.y_actual.keys())[k] for k in indexes]
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.y_actual.keys()))
        if self.shuffle:
            np.random.shuffle(self.indexes)