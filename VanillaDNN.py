import os
import pickle

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import tensorflow as tf

from tensorflow import keras
from tensorflow.keras import layers
from tqdm.keras import TqdmCallback
from sklearn.model_selection import KFold
from SubmitWrapper import *

if 'USER' in os.environ and os.environ['USER'] == 'isears':
    DATA_ROOT = './data'
else:
    DATA_ROOT = '/kaggle/input/osic-pulmonary-fibrosis-progression'


def get_model():
    inputs = keras.Input(shape=(6,))
    partial_NN = inputs

    for idx in range(0, 5):
        partial_NN = layers.Dense(6, activation="relu")(partial_NN)

    outputs = layers.Dense(1, activation="linear")(partial_NN)

    model = keras.Model(inputs=inputs, outputs=outputs, name="GLaDOS")
    # model.summary()

    model.compile(
        loss=osic_custom_loss,
        #optimizer=keras.optimizers.RMSprop(),
        optimizer="Nadam",
        metrics=["mean_squared_error"]
    )

    return model


# Preprocess and normalize the tabular data
df_train = pd.read_csv(DATA_ROOT + "/train.csv")
X, Y = extract_features_train(df_train)

# Cross validation
kfold = KFold(n_splits=FOLD_COUNT, shuffle=True)
fold_idx = 0
bestscore = 1000
all_scores = list()

for train, test in kfold.split(X, Y):
    curr_model = get_model()
    history = curr_model.fit(X[train], Y[train], epochs=500, verbose=0, callbacks=[TqdmCallback(verbose=0)])
    scores = curr_model.evaluate(X[test], Y[test])
    print(f'Score for fold {fold_idx}: {scores[0]}')

    curr_model.save(f'./models/VanillaDNN-{fold_idx}.h5')

    if scores[0] < bestscore:
        print("[+] Best-yet model, saving...")
        bestscore = scores[0]
        curr_model.save("./models/VanillaDNN-best.h5")

    all_scores.append(scores[0])

    fold_idx += 1

print(f'Average cross-validation score: {np.mean(all_scores)}')