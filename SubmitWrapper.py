import tensorflow as tf
import numpy as np
import pandas as pd

import os
import pydicom
import glob


if 'USER' in os.environ and os.environ['USER'] == 'isears':
    DATA_ROOT = './data'
    MODEL_ROOT = './models'
else:
    DATA_ROOT = '/kaggle/input/ '
    MODEL_ROOT = '/kaggle/input/vanilladnnmodel'

MAX_AGE = 120
MAX_FVC = 4000
MAX_WEEK = 133
MIN_WEEK = -12
FOLD_COUNT = 10


def osic_custom_loss(y_true, y_pred):
    # De-normalize
    y_true = y_true * float(MAX_FVC)
    y_pred = y_pred * float(MAX_FVC)

    delta = tf.minimum(tf.abs(y_true - y_pred), 1000.)
    metric = -(tf.sqrt(2.) * delta) / 250. - tf.math.log(tf.sqrt(2.) * 250.)

    return - metric  # Inverse because trying to get closer to zero?


def extract_lung_vol(pt_id):
    """
    Very rough estimate of lung volume

    :param pt_id: Patient's ID
    :return: float estimate of lung volume (mm^3?)
    """
    slices = [pydicom.read_file(x) for x in glob.glob(f'{DATA_ROOT}/train/{pt_id}/*.dcm')]

def extract_features_test(df_in):
    X = np.zeros((df_in.shape[0] * (13 + 133), 6))
    expanded_df = pd.DataFrame(columns=["Patient_Week", "FVC", "Confidence"])
    x_idx = 0

    for pt in df_in.Patient.unique():
        pt_df = df_in[df_in.Patient == pt]

        for index, row in pt_df.iterrows():

            for week_idx in range(MIN_WEEK, MAX_WEEK + 1):
                expanded_df = expanded_df.append({
                    "Patient_Week": pt + "_" + str(week_idx),
                    "FVC": 0,  # placeholder
                    "Confidence": 250  # placeholder ("magic" confidence)
                }, ignore_index=True)

                # Normalize age
                age_normalized = row['Age'] / MAX_AGE

                # Normalize gender
                if row['Sex'] == 'Male':
                    sex_normalized = 1
                elif row['Sex'] == 'Female':
                    sex_normalized = 0
                else:
                    assert False, "Bad entry in Sex column: {}".format(row['Sex'])

                # Normalize smoker status
                if row['SmokingStatus'] == 'Currently smokes':
                    smoke_normalized = 1
                elif row['SmokingStatus'] == 'Ex-smoker':
                    smoke_normalized = 0.5
                elif row['SmokingStatus'] == 'Never smoked':
                    smoke_normalized = 0
                else:
                    assert False, "Bad entry in SmokingStatus column: {}".format(row['SmokingStatus'])

                # Normalize baseline %
                percent_normalized = row['Percent'] / 100

                # Normalize baseline FVC
                fvc_normalized = row['FVC'] / MAX_FVC

                # Compute week relative to earliest FVC measurement and normalize
                week_normalized = week_idx / MAX_WEEK

                X[x_idx] = np.array([age_normalized, sex_normalized, smoke_normalized, percent_normalized, fvc_normalized, week_normalized])

                x_idx += 1

    return X, expanded_df


def extract_features_train(df_in):
    # Input matrix will be (# of entries in tabular data set x # of features)
    # Features:
    # (0) Age (1) Gender (2) Smoker-status (3) Baseline % (4) Week #
    #
    # Note:
    # Baseline % comes from percent column corresponding to EARLIEST (>= 0) WEEK
    # Week # is calculated as (Weeks column) - (EARLIEST (>= 0) WEEK)

    X = np.zeros((df_in.shape[0], 6))
    idx = 0

    # For now, just outputting a prediction. Will worry about confidence later
    Y = np.zeros((df_in.shape[0], 1))

    for pt in df_in.Patient.unique():
        pt_df = df_in[df_in.Patient == pt]
        # print(pt_df.head(5))

        # Get earliest positive week and get baseline % (same for every entry for this patient)
        earliest_week_after_baseline = pt_df[pt_df.Weeks == pt_df[pt_df.Weeks >= 0].Weeks.min()].Weeks.values[0]
        earliest_week_after_baseline_percent = pt_df[pt_df.Weeks == earliest_week_after_baseline].Percent.values[0]
        earliest_week_after_baseline_fvc = pt_df[pt_df.Weeks == earliest_week_after_baseline].FVC.values[0]

        for index, row in pt_df.iterrows():
            # Normalize age
            age_normalized = row['Age'] / MAX_AGE

            # Normalize gender
            if row['Sex'] == 'Male':
                sex_normalized = 1
            elif row['Sex'] == 'Female':
                sex_normalized = 0
            else:
                assert False, "Bad entry in Sex column: {}".format(row['Sex'])

            # Normalize smoker status
            if row['SmokingStatus'] == 'Currently smokes':
                smoke_normalized = 1
            elif row['SmokingStatus'] == 'Ex-smoker':
                smoke_normalized = 0.5
            elif row['SmokingStatus'] == 'Never smoked':
                smoke_normalized = 0
            else:
                assert False, "Bad entry in SmokingStatus column: {}".format(row['SmokingStatus'])

            # Normalize baseline %
            baseline_percent_normalized = earliest_week_after_baseline_percent / 100

            # Normalize baseline FVC
            baseline_fvc_normalized = earliest_week_after_baseline_fvc / MAX_FVC

            # Compute week relative to earliest FVC measurement and normalize
            week_actual = row['Weeks'] - earliest_week_after_baseline
            week_normalized = week_actual / MAX_WEEK

            X[idx] = np.array(
                [age_normalized, sex_normalized, smoke_normalized, baseline_percent_normalized, baseline_fvc_normalized, week_normalized])


            # Compute actual % at this time
            # Y[idx] = row['Percent'] / 100
            Y[idx] = row['FVC'] / MAX_FVC

            idx += 1

    return X, Y


if __name__ == "__main__":
    X_test, submit_holder = extract_features_test(pd.read_csv(DATA_ROOT + "/test.csv"))
    X_train, Y_train = extract_features_train(pd.read_csv(DATA_ROOT + "/train.csv"))

    # Get results from all models
    res = np.zeros((len(submit_holder), 1))
    for idx in range(0, FOLD_COUNT):
        model = tf.keras.models.load_model(MODEL_ROOT + f"/VanillaDNN-{idx}.h5", custom_objects={'osic_custom_loss': osic_custom_loss})
        scores = model.evaluate(X_train, Y_train)
        print(f"Sanity check (training data) score: {scores[0]}")

        res = res + model.predict(X_test)

    # Average all results
    res = res / FOLD_COUNT

    res_idx = 0
    for index, row in submit_holder.iterrows():
        submit_holder.at[index, 'FVC'] = res[res_idx][0] * MAX_FVC  # Need to un-normalize
        res_idx += 1

    print(submit_holder.head(10))

    submit_holder.to_csv("./submission.csv", index=False)