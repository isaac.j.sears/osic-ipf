# Notes

- Use CT scans to extract body fat
    - Need to somehow standardize location (pt. or range) where body fat is measured
    - May be complicated by male vs. female
    
- Patterns in data based on smoker/past smoker/non smoker:
    - Very little consistency in FVCs over time for smokers, some even have upward linear trend
    - Some really nice, clean trends in "never smoked" category, but also occasional low r-squared values
        - Lying about past smoking history? Occupational hazards?
        
- Threshold for usefulness is -8.023182671302157
    - Any score less than this is doing worse than just taking the mean and std-dev of FVC
    
- Outliers / Bad Data:
    - ID00219637202258203123958 FVC values way too high